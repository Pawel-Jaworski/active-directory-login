package pl.net.innotec.activedirectorylogin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ActiveDirectoryLoginApplication {

    public static void main(String[] args) {
        SpringApplication.run(ActiveDirectoryLoginApplication.class, args);
    }

}
